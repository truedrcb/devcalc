// DevCalc.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "DevCalc.h"
#include "CalcWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

struct SCFG CFG=
{
"DevCalc 2.00 config file. Creator - DRCB. 2000 ",

0,0,0,0,SW_NORMAL,

 {               // ����� ����� (��. LOGFONT)
 15,             //LONG lfHeight;     //������ �������
 0,              //LONG lfWidth;      //������ �������=0
 0,              //LONG lfEscapement; //���� ������� 
 0,              //LONG lfOrientation;//���� ����� (����� ������� =0 ��� =lfEscapement)
 FW_BOLD,        //LONG lfWeight;     //Bold'������
 0,              //BYTE lfItalic;     //Italic
 0,              //BYTE lfUnderline;  //�������������
 0,              //BYTE lfStrikeOut;  //������������
 RUSSIAN_CHARSET,//BYTE lfCharSet;    //����� ��������
 OUT_DEFAULT_PRECIS,//BYTE lfOutPrecision; 
 CLIP_DEFAULT_PRECIS,//BYTE lfClipPrecision;
 PROOF_QUALITY,  //BYTE lfQuality;    //��������
 DEFAULT_PITCH|FF_DONTCARE,//BYTE lfPitchAndFamily; 
 "Arial"         //TCHAR lfFaceName[LF_FACESIZE]; //�������� �����
 },

SCFG::SHOW_DEC|SCFG::SHOW_TITLE|SCFG::SHOW_BORDER, // ������� ����� �� �����
0,  // ����� ��������� ����� ����������� ���������� �� Enter
  {"",""} // ������ �����
};


/////////////////////////////////////////////////////////////////////////////
// CDevCalcApp

BEGIN_MESSAGE_MAP(CDevCalcApp, CWinApp)
	//{{AFX_MSG_MAP(CDevCalcApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDevCalcApp construction

CDevCalcApp::CDevCalcApp()
{
sCFGFileName[0]=0;
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CDevCalcApp object

CDevCalcApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CDevCalcApp initialization

BOOL CDevCalcApp::InitInstance()
{
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

m_pszAppName=_tcsdup(_T("Developers Calculator 2.00"));

 {
 char drive[_MAX_DRIVE];
 char dir[_MAX_DIR];
 char fname[_MAX_FNAME];
 char ext[_MAX_EXT];
 _splitpath(m_pszHelpFilePath,drive,dir,fname,ext);
 _makepath(sCFGFileName,drive,dir,fname,".cfg");
 FILE *id=fopen(sCFGFileName,"rb");
 if(id)
  {
  fread(&CFG,1,sizeof(CFG),id);
  fclose(id);
  }
 }

m_nCmdShow=CFG.MainWnd_cmdShow;


m_pMainWnd=new CCalcWnd;
m_pMainWnd->ShowWindow(m_nCmdShow);
m_pMainWnd->UpdateWindow();

// Since the dialog has been closed, return FALSE so that we exit the
//  application, rather than start the application's message pump.
return TRUE;
}

int CDevCalcApp::ExitInstance() 
{
FILE *id=fopen(sCFGFileName,"wb");
if(id)
 {
 fwrite(&CFG,1,sizeof(CFG),id);
 fclose(id);
 }
return CWinApp::ExitInstance();
}
