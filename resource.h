//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by DevCalc.rc
//
#define ID_MENU                         2
#define ID_CHANGE_FONT                  3
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define ID_SHOW_DEC                     102
#define ID_SHOW_HEX                     103
#define ID_SHOW_OCT                     104
#define ID_SHOW_BIN                     105
#define ID_SHOW_TITLE                   106
#define ID_SHOW_CLASSIC1                107
#define ID_SHOW_BORDER                  108
#define ID_SHOW_DIVIDERS                109
#define ID_SHOW_TOP                     110
#define ID_SELECT_DEC                   114
#define ID_SELECT_HEX                   115
#define ID_SELECT_OCT                   116
#define ID_SELECT_BIN                   117
#define ID_MAINCOMBO                    120
#define IDR_MAINFRAME                   128
#define ID_CALCULATE                    136
#define ID_CALC_DEC                     140
#define ID_CALC_HEX                     141
#define ID_CALC_OCT                     142
#define ID_CALC_BIN                     143
#define IDR_METAFILE_X                  143
#define IDR_METAFILE_SQRT               144
#define IDR_METAFILE_V                  145
#define IDR_METAFILE_TITLE              147
#define IDD_ABOUTDIALOG                 148
#define ID_CLASSIC_FIRST                200
#define IDC_1                           1000
#define IDC_2                           1001
#define IDC_3                           1002
#define IDS_ABOUT                       57666

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        149
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           142
#endif
#endif
