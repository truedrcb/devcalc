// XLayoutDialog.cpp : implementation file
//

#include "stdafx.h"
#include "DevCalc.h"
#include "XLayoutDialog.h"
#include "CalcWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CXLayoutDialog dialog


CXLayoutDialog::CXLayoutDialog(CFont *Font, int iButtonWid, CWnd* pParent /*=NULL*/)
	: CDialog(CXLayoutDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CXLayoutDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
m_Font=Font;
m_iButtonWid=iButtonWid;
}


void CXLayoutDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CXLayoutDialog)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CXLayoutDialog, CDialog)
	//{{AFX_MSG_MAP(CXLayoutDialog)
	ON_WM_NCHITTEST()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


void CXLayoutDialog::CenterWindow()
{
UINT mx=layout.GetHRuler().GetMinWidth();
UINT my=layout.GetVRuler().GetMinWidth();
CRect dr;
GetDesktopWindow()->GetClientRect(&dr);
CSize s(mx,my);
CPoint p=dr.CenterPoint();
p.Offset(-((int)mx/2),-((int)my/2));
CRect r(p,s);
MoveWindow(&r);
}


/////////////////////////////////////////////////////////////////////////////
// CXLayoutDialog message handlers


UINT CXLayoutDialog::OnNcHitTest(CPoint point) 
{
CRect r;
GetWindowRect(&r);
if(r.PtInRect(point))
 {
 return HTCAPTION;
 }
return HTNOWHERE;
//return CDialog::OnNcHitTest(point);
}

void CXLayoutDialog::OnSize(UINT nType, int cx, int cy) 
{
CDialog::OnSize(nType, cx, cy);

CRect r;
GetClientRect(&r);
layout.Resize(r);
}

BOOL CXLayoutDialog::OnInitDialog() 
{
EnableToolTips();

CDialog::OnInitDialog();

CRect r;
GetClientRect(&r);

layout.Create(4,4,WS_VISIBLE,r,this,CXLayoutDialog::IDD);

int bw=GetSystemMetrics(SM_CXSIZEFRAME);
layout.GetHRuler().SetCellWidth(0,bw);
layout.GetHRuler().SetCellWeight(1,1);
layout.GetHRuler().SetCellMinWidth(1,GetSystemMetrics(SM_CXSCREEN)/2);
layout.GetHRuler().SetCellWidth(2,m_iButtonWid);
layout.GetHRuler().SetCellWidth(3,bw);

layout.GetVRuler().SetCellWidth(0,bw);
layout.GetVRuler().SetCellWidth(1,m_iButtonWid);
layout.GetVRuler().SetCellWidth(2,m_iButtonWid*3);
layout.GetVRuler().SetCellWidth(3,bw);

//s1.Create("DevCalc...\ndkj",WS_VISIBLE,r,&layout);
//layout.Add(1,2,s1.m_hWnd);

m_TitleStatic.Create(IDR_METAFILE_TITLE,WS_VISIBLE|WS_CHILD|WS_DISABLED|XB_LEFT,r,this,IDOK);
layout.Add(1,1,m_TitleStatic.m_hWnd);

m_ExitButton.Create(IDR_METAFILE_X,WS_VISIBLE|WS_CHILD,r,this,IDOK);
layout.Add(2,1,m_ExitButton.m_hWnd);

CString s;
s.LoadString(IDS_ABOUT);

m_ContentStatic.Create(s,WS_VISIBLE|WS_CHILD|WS_DISABLED|XB_LEFT,r,this,IDOK);
layout.Add(1,2,m_ContentStatic.m_hWnd);

if(!m_Font)m_Font=GetFont();
layout.SetFont(m_Font,FALSE);
m_ContentStatic.SetFont(m_Font,FALSE);
//layout.Resize(r);
layout.EnableToolTips();
CenterWindow();


return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
