#if !defined(AFX_XBUTTON_H__7956B323_E400_4F46_9E8B_797C32CFE160__INCLUDED_)
#define AFX_XBUTTON_H__7956B323_E400_4F46_9E8B_797C32CFE160__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// XButton.h : header file
//

#include "MetaFile.h"

/////////////////////////////////////////////////////////////////////////////
// CXButton window

#define XB_TYPEMASK         0x0000001FL
#define XB_ALIGN            0x00000007L
#define XB_FIT              0x00000000L
#define XB_LEFT             0x00000001L
#define XB_RIGHT            0x00000002L
#define XB_STRETCH          0x00000003L


class CXButton : public CWnd
{
int mx,my;
BOOLEAN mi,mb,state;
HFONT m_hFont;
CMetaFile m_MetaFile;

// Construction
public:
	CXButton();


// Attributes
public:

// Operations
public:

virtual BOOL Create(LPCTSTR lpszWindowName,DWORD dwStyle,const RECT& rect, CWnd* pParentWnd,UINT nID);
virtual BOOL CXButton::Create(
 UINT rIDMetafile,
 LPCTSTR lpszWindowName,
 DWORD dwStyle,
 const RECT& rect,
 CWnd* pParentWnd,
 UINT nID);
virtual BOOL CXButton::Create(
 UINT rIDMetafile,
 DWORD dwStyle,
 const RECT& rect,
 CWnd* pParentWnd,
 UINT nID);

void SetState( BOOL bHighlight);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXButton)
	protected:
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CXButton();

	// Generated message map functions
protected:
	//{{AFX_MSG(CXButton)
	afx_msg void OnPaint();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnCaptureChanged(CWnd *pWnd);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XBUTTON_H__7956B323_E400_4F46_9E8B_797C32CFE160__INCLUDED_)
