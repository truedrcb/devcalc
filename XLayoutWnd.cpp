// XLayoutWnd.cpp : implementation file
//

#include "stdafx.h"
#include "DevCalc.h"
#include "XLayoutWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CXLayoutWnd

CXLayoutWnd::CXLayoutWnd()
{
}

CXLayoutWnd::~CXLayoutWnd()
{
}


BEGIN_MESSAGE_MAP(CXLayoutWnd, CWnd)
	//{{AFX_MSG_MAP(CXLayoutWnd)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
   ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTW, 0, 0xFFFF, OnToolTipNotify)
   ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTA, 0, 0xFFFF, OnToolTipNotify)

END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CXLayoutWnd message handlers

void CXLayoutWnd::Create(UINT xnum,UINT ynum,
						 DWORD dwStyle,const RECT &rect,CWnd *pParentWnd,UINT nID)
{
CWnd::Create(_T("STATIC"),"",WS_CHILD|dwStyle,rect,pParentWnd,nID);
InitLayout(xnum,ynum);
}

void CXLayoutWnd::InitLayout(UINT xnum,UINT ynum)
{
CRect cr(0,0,0,0);
if(IsWindow(m_hWnd))GetClientRect(&cr);
layout.Create(xnum,ynum,cr);
}

void CXLayoutWnd::Add(UINT x,UINT y,UINT w,UINT h,HWND window,UINT addh)
{
layout.Add(x,y,w,h,window,addh);
}

void CXLayoutWnd::Add(UINT x,UINT y,HWND window,UINT addh)
{
layout.Add(x,y,1,1,window,addh);
}

void CXLayoutWnd::Move(UINT x,UINT y,UINT w,UINT h,HWND window,UINT addh)
{
layout.Move(x,y,w,h,window,addh);
}

void CXLayoutWnd::Move(UINT x,UINT y,HWND window,UINT addh)
{
layout.Move(x,y,1,1,window,addh);
}

void CXLayoutWnd::OnSize(UINT nType, int cx, int cy) 
{
CWnd::OnSize(nType, cx, cy);
CRect cr;
GetClientRect(&cr);
Resize(cr); 
}

void CXLayoutWnd::Resize(const RECT rect) 
{
layout.Resize(rect);
layout.ReplaceItems(FALSE);
}

LRESULT CXLayoutWnd::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
if(message==WM_SETFONT)SendMessageToDescendants(message,wParam,lParam);
else
if(message==WM_COMMAND)GetParent()->PostMessage(message,wParam,lParam);

return CWnd::DefWindowProc(message, wParam, lParam);
}

BOOL CXLayoutWnd::OnToolTipNotify(UINT id, NMHDR *pNMHDR,LRESULT *pResult)
{
// need to handle both ANSI and UNICODE versions of the message
TOOLTIPTEXTA* pTTTA = (TOOLTIPTEXTA*)pNMHDR;
TOOLTIPTEXTW* pTTTW = (TOOLTIPTEXTW*)pNMHDR;
CString strTipText;
UINT nID = pNMHDR->idFrom;
if (pNMHDR->code == TTN_NEEDTEXTA && (pTTTA->uFlags & TTF_IDISHWND) ||
	pNMHDR->code == TTN_NEEDTEXTW && (pTTTW->uFlags & TTF_IDISHWND))
 {
 // idFrom is actually the HWND of the tool
 nID = ::GetDlgCtrlID((HWND)nID);
 }

if (nID != 0) // will be zero on a separator
 {
 if(!strTipText.LoadString(nID))strTipText.Format("Control ID = %d",nID);
 }

if (pNMHDR->code == TTN_NEEDTEXTA)
lstrcpyn(pTTTA->szText, strTipText, sizeof(pTTTA->szText));
else
_mbstowcsz(pTTTW->szText, strTipText, sizeof(pTTTW->szText));
*pResult = 0;

return TRUE;    // message was handled
}

