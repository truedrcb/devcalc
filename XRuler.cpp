// XRuler.cpp: implementation of the CXRuler class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DevCalc.h"
#include "XRuler.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CXRuler::CXRuler()
{
cells=NULL;
size=0;
width=0;
}

CXRuler::~CXRuler()
{
Close();
}

void CXRuler::Close()
{
if(cells)delete cells;
size=0;
width=0;
}

void CXRuler::Create(UINT size)
{
Close();

cells=new CXRuler::SCell[size];

ASSERT(cells!=NULL);

CXRuler::size=size;

for(UINT f=0;f<size;f++)
 {
 cells[f].visible=TRUE;
 cells[f].weight=0;
 cells[f].width=0;
 cells[f].minwidth=0;
 }

}

void CXRuler::SetCellWidth(UINT index,UINT width)
{
ASSERT(index<size);

cells[index].width=width;
cells[index].minwidth=width;
cells[index].weight=0;

CalcW();
}

void CXRuler::SetCellMinWidth(UINT index,UINT width)
{
ASSERT(index<size);

cells[index].minwidth=width;

CalcW();
}

void CXRuler::SetCellWeight(UINT index,UINT weight)
{
ASSERT(index<size);

cells[index].width=0;
cells[index].weight=weight;

CalcW();
}

void CXRuler::SetCellVisible(UINT index,BOOLEAN visible)
{
ASSERT(index<size);

cells[index].visible=visible;

CalcW();
}


UINT CXRuler::GetMinWidth()
{
UINT minwid=0;
for(UINT f=0;f<size;f++)
 if(cells[f].visible)minwid+=cells[f].minwidth;

return minwid;
}


UINT CXRuler::GetCellWeight(UINT index)
{
ASSERT(index<size);
return cells[index].weight;
}

UINT CXRuler::GetCellWidth(UINT index)
{
ASSERT(index<size);
return cells[index].width;
}

UINT CXRuler::GetCellW(UINT index)
{
ASSERT(index<size);
return cells[index].w;
}

UINT CXRuler::GetCellX(UINT index)
{
ASSERT(index<size);
UINT x=0;
//index--;
for(UINT f=0;f<index;f++)x+=cells[f].w;
return x;
}


UINT CXRuler::GetCellW(UINT index,UINT num)
{
ASSERT((index+num)<=size);
UINT w=cells[index].w;
for(UINT f=1;f<num;f++)w+=cells[index+f].w;
return w;
}


UINT CXRuler::GetWidth()
{
return CXRuler::width;
}

void CXRuler::SetWidth(UINT width)
{
CXRuler::width=width;
CalcW();
}


void CXRuler::CalcW()
{
UINT f;
UINT minwid=0;
UINT fullwei=0;

for(f=0;f<size;f++)
 {
 if(cells[f].visible)
  {
  minwid+=(cells[f].w=cells[f].width);
  fullwei+=cells[f].weight;
  }
 else cells[f].w=0;
 }

if(minwid>=width||fullwei==0)return;

minwid=width-minwid;

UINT curwei=0;
for(f=0;f<size;f++)
 {
 if(cells[f].visible)
  {
  UINT wei=cells[f].weight;
  if(wei>0)
   {
   wei+=curwei;
   cells[f].w=minwid*wei/fullwei-minwid*curwei/fullwei;
   curwei=wei;
   }
  }
 }
}
