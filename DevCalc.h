// DevCalc.h : main header file for the DEVCALC application
//

#if !defined(AFX_DEVCALC_H__E33D1185_EB69_11D3_B481_0000E8550785__INCLUDED_)
#define AFX_DEVCALC_H__E33D1185_EB69_11D3_B481_0000E8550785__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

long double Calculator(const char *Expression);
extern int CalculatorError;
int LDouble2String(long double src,char *dest,int maxsize,int base);

struct SCFG
{
char id[100];

int MainWnd_cx;
int MainWnd_cy;
int MainWnd_x;
int MainWnd_y;
UINT MainWnd_cmdShow;

LOGFONT Font;

UINT show;
enum 
 {
 SHOW_DEC=     0x0001,
 SHOW_HEX=     0x0002,
 SHOW_OCT=     0x0004,
 SHOW_BIN=     0x0008,
 SHOW_TITLE=   0x0010,
 SHOW_CLASSIC1=0x0020, 
 SHOW_BORDER=  0x0040, 
 SHOW_DIVIDERS=0x0080, 
 SHOW_TOP=     0x0100, 
 SHOW_STATUS= 0x0000,
 SHOW_BUTTONS=0x0000
 };

int form;

char string[20][101];
};

extern struct SCFG CFG;

/////////////////////////////////////////////////////////////////////////////
// CDevCalcApp:
// See DevCalc.cpp for the implementation of this class
//

class CDevCalcApp : public CWinApp
{
char sCFGFileName[_MAX_PATH];
public:
	CDevCalcApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDevCalcApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CDevCalcApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEVCALC_H__E33D1185_EB69_11D3_B481_0000E8550785__INCLUDED_)
