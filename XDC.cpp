// XDC.cpp: implementation of the CXDC class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DevCalc.h"
#include "XDC.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CXDC::CXDC()
{
paintDC=NULL;
omemBitmap=NULL;
}

CXDC::~CXDC()
{
if(omemBitmap)SelectObject(omemBitmap);
memBitmap.DeleteObject();
}

CXDC::CXDC(CWnd *paintClient,CDC *paintDC)
{
ASSERT(paintClient);
ASSERT(paintDC);

CXDC::paintDC=paintDC;

paintClient->GetClientRect(&clientRect);

memBitmap.CreateCompatibleBitmap(paintDC,clientRect.Width(),clientRect.Height());
CreateCompatibleDC(paintDC);
omemBitmap=SelectObject(&memBitmap);
}

void CXDC::show(void)
{
ASSERT(paintDC);
paintDC->BitBlt(clientRect.left,clientRect.top,
				clientRect.Width(),clientRect.Height(),this,0,0,SRCCOPY);
}
