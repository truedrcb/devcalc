#if !defined(AFX_XLAYOUTWND_H__74D9DCC9_17F6_42B7_9327_6ACD77A41891__INCLUDED_)
#define AFX_XLAYOUTWND_H__74D9DCC9_17F6_42B7_9327_6ACD77A41891__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// XLayoutWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CXLayoutWnd window

#include "XRulerLayout.h"

class CXLayoutWnd : public CWnd
{
CXRulerLayout layout;

// Construction
public:
CXLayoutWnd();
void Create(UINT xnum,UINT ynum,
		 DWORD dwStyle,const RECT &rect,CWnd *pParentWnd,UINT nID);

// Attributes
public:

// Operations
public:

void InitLayout(UINT xnum,UINT ynum);
void Add(UINT x,UINT y,UINT w,UINT h,HWND window,UINT addh=0);
void Add(UINT x,UINT y,HWND window,UINT addh=0);
void Move(UINT x,UINT y,UINT w,UINT h,HWND window,UINT addh=0);
void Move(UINT x,UINT y,HWND window,UINT addh=0);
void Resize(const RECT rect);

CXRuler & GetHRuler()
 {
 return layout.GetHRuler();
 };

CXRuler & GetVRuler()
 {
 return layout.GetVRuler();
 };

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXLayoutWnd)
	protected:
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL


// Implementation
public:
virtual ~CXLayoutWnd();

	// Generated message map functions
protected:
//{{AFX_MSG(CXLayoutWnd)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG

afx_msg BOOL OnToolTipNotify(UINT id, NMHDR *pNMHDR,LRESULT *pResult);

DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XLAYOUTWND_H__74D9DCC9_17F6_42B7_9327_6ACD77A41891__INCLUDED_)
