// XRulerLayout.h: interface for the CXRulerLayout class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_XRULERLAYOUT_H__B462B95B_0B5D_4706_B106_494753EF91C6__INCLUDED_)
#define AFX_XRULERLAYOUT_H__B462B95B_0B5D_4706_B106_494753EF91C6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "XRuler.h"

class CXRulerLayout  
{
struct SItem
 {
 UINT x,y,w,h;
 UINT addh;
 HWND window;
 SItem *next;
 BOOLEAN visible;
 };

struct SItem *first;

CXRuler horizontal;
CXRuler vertical;
CRect rect; 

public:
CXRulerLayout();
virtual ~CXRulerLayout();

void Create(UINT xnum,UINT ynum,const RECT rect);
void Add(UINT x,UINT y,UINT w,UINT h,HWND window,UINT addh=0);
void Move(UINT x,UINT y,UINT w,UINT h,HWND window,UINT addh);
void Move(UINT x,UINT y,UINT w,UINT h,HWND window);


CXRuler & GetHRuler()
 {
 return horizontal;
 };

CXRuler & GetVRuler()
 {
 return vertical;
 };

void ReplaceItems(BOOL bRepaint=TRUE);
void Resize(const RECT rect);

};

#endif // !defined(AFX_XRULERLAYOUT_H__B462B95B_0B5D_4706_B106_494753EF91C6__INCLUDED_)
