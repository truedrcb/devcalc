// CalcWnd.cpp : implementation file
//

#include "stdafx.h"
#include "DevCalc.h"
#include "CalcWnd.h"
#include "XLayoutDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCalcWnd

struct CCalcWnd::SClassicButtonFunction 
 CalcClassicButtons[]=
 {
  {"Sin",0,"sin(",")"},
  {"Cos",0,"cos(",")"},
  {"Tan",0,"tan(",")"},
  {"Ln",0,"log(",")"},
  {"Log10",0,"log10(",")"},
  {"aSin",0,"asin(",")"},
  {"aCos",0,"acos(",")"},
  {"aTan",0,"atan(",")"},
  {"Exp",0,"exp(",")"},
  {"",IDR_METAFILE_SQRT,"sqrt(",")"},
  {"Sinh",0,"sinh(",")"},
  {"Cosh",0,"cosh(",")"},
  {"Tanh",0,"tanh(",")"},
  {"Abs",0,"abs(",")"},
  {"Not",0,"not(",")"},
  {"\xb0 \xbb rad",0,"degtorad(",")"},
  {"rad \xbb \xb0",0,"radtodeg(",")"},
  {"1/x",0,"1/(",")"}
 };

CCalcWnd::CCalcWnd()
{

m_bInitialized=FALSE;

int f;

for(f=0;f<4;f++)
 {
 m_pRezButton[f]=new CXButton;
 m_pSelectButton[f]=new CXButton;
 }

CRect rect(
 CFG.MainWnd_x,
 CFG.MainWnd_y,
 CFG.MainWnd_x+CFG.MainWnd_cx,
 CFG.MainWnd_y+CFG.MainWnd_cy
 );


//TRACE("\nCFG.MainWnd_x=%i CFG.MainWnd_y=%i CFG.MainWnd_cx=%i CFG.MainWnd_cy=%i\n",
//	  CFG.MainWnd_x,CFG.MainWnd_y,CFG.MainWnd_cx,CFG.MainWnd_cy);

//TRACE("screen %ix%i\n",GetSystemMetrics(SM_CXSCREEN),GetSystemMetrics(SM_CYSCREEN));

// check if window is out of screen
if((CFG.MainWnd_x+CFG.MainWnd_cx/2)<0||(CFG.MainWnd_y+CFG.MainWnd_cy/2)<0||
 (CFG.MainWnd_x+CFG.MainWnd_cx/2)>GetSystemMetrics(SM_CXSCREEN)||
 (CFG.MainWnd_y+CFG.MainWnd_cy/2)>GetSystemMetrics(SM_CYSCREEN)
 )
 CFG.MainWnd_cx=0;

// if window position is wrong or wasn't stored
// set default position and center window on the screen
if(CFG.MainWnd_cx<=0||CFG.MainWnd_cy<=0)
 {
 int cx=GetSystemMetrics(SM_CXFULLSCREEN)/2;
 int cy=GetSystemMetrics(SM_CYFULLSCREEN)/2;
 RECT r={cx-200,cy-30,cx+200,cy+30};
 rect=r;
 }

m_iBordWid=GetSystemMetrics(SM_CXSIZEFRAME);

m_hAccel=LoadAccelerators(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDR_MAINFRAME));

HICON hicon=LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDR_MAINFRAME));

CreateEx(WS_EX_LEFT|WS_EX_APPWINDOW,AfxRegisterWndClass(CS_DBLCLKS,0,0,hicon),
		 "DevCalc 2",WS_POPUP|WS_VISIBLE,rect,0,0);


EnableToolTips(TRUE);

GetClientRect(&rect);

layout.Create(8,15,rect);

layout.GetHRuler().SetCellWidth(0,m_iBordWid);
layout.GetHRuler().SetCellWeight(1,1);
layout.GetHRuler().SetCellWidth(2,m_iBordWid);
layout.GetHRuler().SetCellWidth(4,m_iBordWid);
layout.GetHRuler().SetCellWidth(6,m_iBordWid);
layout.GetHRuler().SetCellWidth(7,m_iBordWid+2);

for(f=0;f<15;f+=2)layout.GetVRuler().SetCellWidth(f,m_iBordWid);

layout.GetVRuler().SetCellWidth(1,18);

layout.GetVRuler().SetCellWidth(13,100);

CRect temprect(0,0,0,0);

m_ClassicWnd.Create(5,4,WS_VISIBLE,temprect,this,ID_SHOW_CLASSIC1);
m_ClassicWnd.EnableToolTips(TRUE);

layout.Add(1,13,5,1,m_ClassicWnd.m_hWnd);

for(f=0;f<5;f++)m_ClassicWnd.GetHRuler().SetCellWeight(f,1);
for(f=0;f<4;f++)m_ClassicWnd.GetVRuler().SetCellWeight(f,1);

////////////////
for(f=0;f<(sizeof(CalcClassicButtons)/sizeof(CalcClassicButtons[0]));f++)
 {
 CXButton *b1=new CXButton;
 b1->Create(
   CalcClassicButtons[f].rID,
   CalcClassicButtons[f].name,
   WS_CHILD|WS_VISIBLE,
   temprect,
   &m_ClassicWnd,
   ID_CLASSIC_FIRST+f);
 m_ClassicWnd.Add(f%5,f/5,b1->m_hWnd);
 //delete b1;
 }


m_TitleStatic.Create(IDR_METAFILE_TITLE,WS_VISIBLE|WS_CHILD|WS_DISABLED|XB_LEFT,temprect,this,IDR_MAINFRAME);
layout.Add(1,1,1,1,m_TitleStatic.m_hWnd);

m_Combo.Create(WS_CHILD|WS_VISIBLE|WS_VSCROLL|CBS_DROPDOWN,temprect,this,
 ID_MAINCOMBO);
m_Combo.InitStorage(20,100);
FillCombo();
layout.Add(1,3,1,1,m_Combo.m_hWnd,10);


m_ExitButton.Create(IDR_METAFILE_X,BS_PUSHBUTTON|WS_VISIBLE|WS_CHILD,temprect,this,ID_APP_EXIT);
layout.Add(5,3,1,1,m_ExitButton.m_hWnd);


m_MenuButton.Create(IDR_METAFILE_V,BS_PUSHBUTTON|WS_VISIBLE|WS_CHILD,temprect,this,ID_MENU);
layout.Add(3,3,1,1,m_MenuButton.m_hWnd);

static char *SelectTxt[4]={"Dec","Hex","Oct","Bin"};

for(f=0;f<4;f++)
 {
 m_pRezButton[f]->Create("",BS_PUSHBUTTON|WS_CHILD|WS_VISIBLE,temprect,this,ID_CALC_DEC+f);
 
 layout.Add(1,f*2+5,1,1,m_pRezButton[f]->m_hWnd);

 m_pSelectButton[f]->Create(SelectTxt[f],BS_PUSHBUTTON|WS_CHILD|WS_VISIBLE,temprect,this,
  ID_SELECT_DEC+f);
 m_pSelectButton[f]->SetState((f==CFG.form)?TRUE:FALSE);

 layout.Add(3,f*2+5,3,1,m_pSelectButton[f]->m_hWnd);
 }


m_bInitialized=TRUE;
CreateShow();
CreateFont();
}


CCalcWnd::~CCalcWnd()
{
if(m_hAccel)DestroyAcceleratorTable(m_hAccel);
m_Font.DeleteObject();

for(int f=0;f<4;f++)
 {
 if(m_pRezButton[f])delete m_pRezButton[f];
 if(m_pSelectButton[f])delete m_pSelectButton[f];
 }
}


void CCalcWnd::CreateShow(void)
{

if(CFG.show&SCFG::SHOW_TITLE)
 {
 layout.GetVRuler().SetCellVisible(1);
 layout.GetVRuler().SetCellVisible(2);
 layout.Move(1,3,5,1,m_Combo.m_hWnd);
 layout.Move(5,1,1,1,m_ExitButton.m_hWnd);
 layout.Move(3,1,1,1,m_MenuButton.m_hWnd);
 }
else
 {
 layout.GetVRuler().SetCellVisible(1,FALSE);
 layout.GetVRuler().SetCellVisible(2,FALSE);
 layout.Move(1,3,1,1,m_Combo);
 layout.Move(5,3,1,1,m_ExitButton.m_hWnd);
 layout.Move(3,3,1,1,m_MenuButton.m_hWnd);
 }

for(int f=0;f<4;f++)
if(CFG.show&(1<<f))
 {
 layout.GetVRuler().SetCellVisible(f*2+4);
 layout.GetVRuler().SetCellVisible(f*2+5);
 }
else
 {
 layout.GetVRuler().SetCellVisible(f*2+4,FALSE);
 layout.GetVRuler().SetCellVisible(f*2+5,FALSE);
 }

if(CFG.show&SCFG::SHOW_BORDER)
 {
 layout.GetVRuler().SetCellVisible(0);
 layout.GetVRuler().SetCellVisible(14);
 layout.GetHRuler().SetCellVisible(0);
 layout.GetHRuler().SetCellVisible(6);
 }
else
 {
 layout.GetVRuler().SetCellVisible(0,FALSE);
 layout.GetVRuler().SetCellVisible(14,FALSE);
 layout.GetHRuler().SetCellVisible(0,FALSE);
 layout.GetHRuler().SetCellVisible(6,FALSE);
 }

if(CFG.show&SCFG::SHOW_CLASSIC1)
 {
 layout.GetVRuler().SetCellVisible(12);
 layout.GetVRuler().SetCellVisible(13);
 }
else
 {
 layout.GetVRuler().SetCellVisible(12,FALSE);
 layout.GetVRuler().SetCellVisible(13,FALSE);
 }


if(!(CFG.show&SCFG::SHOW_DIVIDERS))
 {
 //if(CFG.show&SCFG::SHOW_TITLE)layout.GetVRuler().SetCellVisible(2,FALSE);
 for(f=0;f<4;f++)
 if(CFG.show&(1<<f))layout.GetVRuler().SetCellVisible(f*2+4,FALSE);
 layout.GetHRuler().SetCellVisible(2,FALSE);
 layout.GetHRuler().SetCellVisible(4,FALSE);
 layout.GetVRuler().SetCellVisible(2,FALSE);
 layout.GetVRuler().SetCellVisible(12,FALSE);
 }
else
 {
 layout.GetHRuler().SetCellVisible(2);
 layout.GetHRuler().SetCellVisible(4);
 }


CRect clir;
GetClientRect(&clir);

CSize size(clir.Width(),layout.GetVRuler().GetMinWidth());
CRect rect(clir.TopLeft(),size);

layout.Resize(rect);
}


void CCalcWnd::CreateFont(void)
{
if((HFONT)m_Font)m_Font.DeleteObject();
m_Font.CreateFontIndirect(&CFG.Font);
SetFont(&m_Font,FALSE);

m_ClassicWnd.SetFont(&m_Font,FALSE); 

m_Combo.SetFont(&m_Font,FALSE); 
m_ExitButton.SetFont(&m_Font,FALSE);

m_MenuButton.SetFont(&m_Font,FALSE);

CRect comborect(0,0,10,100);
m_Combo.ShowWindow(SW_HIDE);
m_Combo.MoveWindow(&comborect,FALSE);
m_Combo.GetWindowRect(&comborect);
int h=comborect.Height();
m_iButtonWid=h;

layout.GetHRuler().SetCellMinWidth(1,h*3);

layout.GetHRuler().SetCellWidth(3,h);
layout.GetHRuler().SetCellWidth(5,h);

layout.GetVRuler().SetCellWidth(1,h);

layout.GetVRuler().SetCellWidth(3,h);

layout.GetVRuler().SetCellWidth(13,h*4);

for(int f=0;f<4;f++)
 {
 m_pRezButton[f]->SetFont(&m_Font,FALSE);
 m_pSelectButton[f]->SetFont(&m_Font,FALSE);
 layout.GetVRuler().SetCellWidth(f*2+5,h);
 }

RecalcLayout();
}


void CCalcWnd::RecalcLayout(void)
{
if(!m_bInitialized)return;
CRect oldwinrect;
GetWindowRect(&oldwinrect);

CPoint winpoint(oldwinrect.left,oldwinrect.top);
int winwid=oldwinrect.Width();
int minwid=layout.GetHRuler().GetMinWidth();
CSize winsize((winwid>minwid)?winwid:minwid,layout.GetVRuler().GetMinWidth());
CRect winrect(winpoint,winsize);

MoveWindow(&winrect);

layout.ReplaceItems(FALSE);

Invalidate();
UpdateWindow();

GetWindowRect(&oldwinrect);

if(!IsWindow(m_Combo.m_hWnd))return;

if(CFG.show&SCFG::SHOW_TOP)
 SetWindowPos(&wndTopMost,0,0,0,0,SWP_NOSIZE|SWP_NOMOVE);
else
 SetWindowPos(&wndNoTopMost,0,0,0,0,SWP_NOSIZE|SWP_NOMOVE);

m_Combo.ShowWindow(SW_SHOWNORMAL);
m_Combo.SetFocus();
 {
 char txt[200];
 m_Combo.GetWindowText(txt,sizeof(txt));
 Calculate(txt);
 }
}

BEGIN_MESSAGE_MAP(CCalcWnd, CWnd)
	//{{AFX_MSG_MAP(CCalcWnd)
	ON_WM_PAINT()
	ON_WM_NCHITTEST()
	ON_WM_DESTROY()
	ON_COMMAND(ID_APP_EXIT, OnAppExit)
	ON_WM_SIZE()
	ON_COMMAND(ID_MENU, OnMenu)
	ON_COMMAND(ID_CALCULATE, OnCalculate)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_CHANGE_FONT, OnChangeFont)
    ON_COMMAND_RANGE(ID_SHOW_DEC, ID_SHOW_TOP, OnShow)
    ON_COMMAND_RANGE(ID_SELECT_DEC, ID_SELECT_BIN, OnSelect)
    ON_COMMAND_RANGE(ID_CALC_DEC, ID_CALC_BIN, OnCalc)
    ON_COMMAND_RANGE(ID_CLASSIC_FIRST, ID_CLASSIC_FIRST+(sizeof(CalcClassicButtons)/sizeof(CalcClassicButtons[0])), OnClassicButton)
	ON_CBN_SELCHANGE(ID_MAINCOMBO, OnComboSelChange)
	ON_CBN_EDITUPDATE(ID_MAINCOMBO, OnComboChange)

   ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTW, 0, 0xFFFF, OnToolTipNotify)
   ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTA, 0, 0xFFFF, OnToolTipNotify)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CCalcWnd message handlers

void CCalcWnd::OnPaint() 
{
CPaintDC dc(this); // device context for painting
	
RECT r;
GetClientRect(&r);
/*
CBitmap bm,mask;
bm.LoadBitmap(IDB_BITMAP1);
CSize bms=bm.GetBitmapDimension();
CDC bdc;
bdc.CreateCompatibleDC(&dc);
CBitmap *obm=bdc.SelectObject(&bm);
dc.StretchBlt(r.left,r.top,r.right-r.left,r.bottom-r.top,
			  &bdc,0,0,73,43,SRCCOPY);

bdc.SelectObject(obm);
bm.DeleteObject();

bm.LoadBitmap(IDB_BITMAP3);
mask.LoadBitmap(IDB_BITMAP4);
obm=bdc.SelectObject(&bm);
//dc.SetStretchBltMode(BLACKONWHITE);

dc.MaskBlt(r.left+5,r.top+5,40,40,
			  &bdc,0,0,mask,0,0,MAKEROP4(SRCCOPY,PATPAINT));
//dc.BitBlt(r.left+5,r.top+5,40,40, &bdc,0,0,SRCCOPY);
bdc.SelectObject(obm);
bm.DeleteObject();
*/
dc.FillSolidRect(&r,GetSysColor(COLOR_3DFACE));
r.right-=m_iBordWid;	
dc.Draw3dRect(&r,GetSysColor(COLOR_3DHILIGHT),GetSysColor(COLOR_3DSHADOW));
r.right+=m_iBordWid;
r.left=r.right-m_iBordWid;
dc.Draw3dRect(&r,GetSysColor(COLOR_3DHILIGHT),GetSysColor(COLOR_3DSHADOW));
}

UINT CCalcWnd::OnNcHitTest(CPoint point) 
{

CRect r;
GetWindowRect(&r);
if(r.PtInRect(point))
 {
 if(point.x>=(r.right-m_iBordWid))return HTRIGHT;
 return HTCAPTION;
 }
return HTNOWHERE;

//return CWnd::OnNcHitTest(point);
}

void CCalcWnd::OnDestroy() 
{
WINDOWPLACEMENT w;
GetWindowPlacement(&w);
CFG.MainWnd_cmdShow=w.showCmd;

GetWindowRect(&w.rcNormalPosition);

CFG.MainWnd_cx=w.rcNormalPosition.right-w.rcNormalPosition.left;
CFG.MainWnd_cy=w.rcNormalPosition.bottom-w.rcNormalPosition.top;
CFG.MainWnd_x=w.rcNormalPosition.left;
CFG.MainWnd_y=w.rcNormalPosition.top;

SaveCombo();

CWnd::OnDestroy();
}

void CCalcWnd::OnAppExit() 
{
DestroyWindow();	
}

void CCalcWnd::OnSize(UINT nType, int cx, int cy) 
{
CWnd::OnSize(nType, cx, cy);

CRect clir;
GetClientRect(&clir);

CSize size(clir.Width(),layout.GetVRuler().GetMinWidth());
CRect rect(clir.TopLeft(),size);

layout.Resize(rect);

RecalcLayout();
}

BOOL CCalcWnd::PreTranslateMessage(MSG* pMsg) 
{
FilterToolTipMessage(pMsg);
if(m_hAccel&&m_hWnd)if(TranslateAccelerator(m_hWnd,m_hAccel,pMsg))return TRUE;
return CWnd::PreTranslateMessage(pMsg);
}

void CCalcWnd::OnMenu() 
{
if(!m_Menu.CreatePopupMenu())return;
m_Menu.AppendMenu(MF_STRING,ID_CALCULATE,"Calculate\tEnter");
m_Menu.SetDefaultItem(ID_CALCULATE);
m_Menu.AppendMenu(MF_SEPARATOR);
m_Menu.AppendMenu(MF_STRING|((CFG.show&SCFG::SHOW_DEC)?MF_CHECKED:MF_UNCHECKED),
				  ID_SHOW_DEC,"Show Dec\tF9");
m_Menu.AppendMenu(MF_STRING|((CFG.show&SCFG::SHOW_HEX)?MF_CHECKED:MF_UNCHECKED),
				  ID_SHOW_HEX,"Show Hex\tF10");
m_Menu.AppendMenu(MF_STRING|((CFG.show&SCFG::SHOW_OCT)?MF_CHECKED:MF_UNCHECKED),
				  ID_SHOW_OCT,"Show Oct\tF11");
m_Menu.AppendMenu(MF_STRING|((CFG.show&SCFG::SHOW_BIN)?MF_CHECKED:MF_UNCHECKED),
				  ID_SHOW_BIN,"Show Bin\tF12");
m_Menu.AppendMenu(MF_SEPARATOR);
m_Menu.AppendMenu(MF_STRING|((CFG.show&SCFG::SHOW_TITLE)?MF_CHECKED:MF_UNCHECKED),
				  ID_SHOW_TITLE,"Show Title\tCtrl+T");
m_Menu.AppendMenu(MF_STRING|((CFG.show&SCFG::SHOW_BORDER)?MF_CHECKED:MF_UNCHECKED),
				  ID_SHOW_BORDER,"Show Border\tCtrl+B");
m_Menu.AppendMenu(MF_STRING|((CFG.show&SCFG::SHOW_DIVIDERS)?MF_CHECKED:MF_UNCHECKED),
				  ID_SHOW_DIVIDERS,"Show Dividers\tCtrl+D");
m_Menu.AppendMenu(MF_STRING|((CFG.show&SCFG::SHOW_TOP)?MF_CHECKED:MF_UNCHECKED),
				  ID_SHOW_TOP,"Always On Top\tCtrl+A");
m_Menu.AppendMenu(MF_STRING|((CFG.show&SCFG::SHOW_CLASSIC1)?MF_CHECKED:MF_UNCHECKED),
				  ID_SHOW_CLASSIC1,"Show function keypad\tCtrl+F");
m_Menu.AppendMenu(MF_SEPARATOR);
m_Menu.AppendMenu(MF_STRING,ID_CHANGE_FONT,"Change Font");
m_Menu.AppendMenu(MF_SEPARATOR);
m_Menu.AppendMenu(MF_STRING,ID_APP_ABOUT,"About DevCalc\tF1");
m_Menu.AppendMenu(MF_SEPARATOR);
m_Menu.AppendMenu(MF_STRING,ID_APP_EXIT,"Exit\tEsc");

RECT r;
m_MenuButton.GetWindowRect(&r);
m_Menu.TrackPopupMenu(TPM_RIGHTALIGN|TPM_RIGHTBUTTON,r.right,r.bottom,this);
m_Menu.DestroyMenu();
m_Combo.SetFocus();
}

void CCalcWnd::OnChangeFont() 
{
CFontDialog d(&CFG.Font,CF_SCREENFONTS);
d.DoModal();
CreateFont();
}

void CCalcWnd::OnShow(UINT nID)
{
int n=nID-ID_SHOW_DEC;
if(n<0||n>8)return;
CFG.show^=1<<n;
CreateShow();
RecalcLayout();
}


void CCalcWnd::Calculate(LPCTSTR txt)
{
for(int f=0;f<4;f++)if(!IsWindow(m_pRezButton[0]->m_hWnd))return;
long double a;
CalculatorError=0;
if(txt[0])a=Calculator(txt);
else a=0;
if(a>0)
 {
 if(a<1e-15)a=0;
 }
else if(a>-1e-15)a=0;

char txt2[100];

if(!CalculatorError)
 {
 // DEC
 //LDouble2String(a,txt,sizeof(txt),20,10);
 sprintf(txt2,"%.15lg",a);
 m_pRezButton[0]->SetWindowText(txt2);
 // HEX
 sprintf(txt2,"0x%X",(int)a);
 m_pRezButton[1]->SetWindowText(txt2);
 // OCT
 txt2[0]='0';
 itoa((int)a,txt2+1,8);
 m_pRezButton[2]->SetWindowText(txt2);
 // BIN
 txt2[0]='0';
 txt2[1]='b';
 itoa((int)a,txt2+2,2);
 m_pRezButton[3]->SetWindowText(txt2);
 }
else
 {
 sprintf(txt2,"Error #%i",(int)CalculatorError);
 for(int f=0;f<4;f++)m_pRezButton[f]->SetWindowText(txt2);
 }
}


void CCalcWnd::OnComboChange()
{
char txt[200];
m_Combo.GetWindowText(txt,sizeof(txt));
Calculate(txt);
}

void CCalcWnd::OnComboSelChange()
{
char txt[200];
m_Combo.GetLBText(m_Combo.GetCurSel(),txt);
Calculate(txt);
}

BOOL CCalcWnd::OnToolTipNotify(UINT id, NMHDR *pNMHDR,LRESULT *pResult)
{
// need to handle both ANSI and UNICODE versions of the message
TOOLTIPTEXTA* pTTTA = (TOOLTIPTEXTA*)pNMHDR;
TOOLTIPTEXTW* pTTTW = (TOOLTIPTEXTW*)pNMHDR;
CString strTipText;
UINT nID = pNMHDR->idFrom;
if (pNMHDR->code == TTN_NEEDTEXTA && (pTTTA->uFlags & TTF_IDISHWND) ||
	pNMHDR->code == TTN_NEEDTEXTW && (pTTTW->uFlags & TTF_IDISHWND))
 {
 // idFrom is actually the HWND of the tool
 nID = ::GetDlgCtrlID((HWND)nID);
 }

if (nID != 0) // will be zero on a separator
 {
 if(!strTipText.LoadString(nID))strTipText.Format("Control ID = %d",nID);
 }

if (pNMHDR->code == TTN_NEEDTEXTA)
lstrcpyn(pTTTA->szText, strTipText, sizeof(pTTTA->szText));
else
_mbstowcsz(pTTTW->szText, strTipText, sizeof(pTTTW->szText));
*pResult = 0;

return TRUE;    // message was handled
}

void CCalcWnd::OnSelect(UINT nID)
{
int n=nID-ID_SELECT_DEC;
if(n<0||n>3)return;
CFG.form=n;
for(int f=0;f<4;f++)
if(m_pSelectButton[f])
 {
 m_pSelectButton[f]->SetState((f==CFG.form)?TRUE:FALSE);
 }
m_Combo.SetFocus();
}


void CCalcWnd::FillCombo(void)
{
m_Combo.ResetContent();
m_Combo.SetWindowText(CFG.string[0]);
for(int f=0;f<20;f++)
 {
 if(!CFG.string[f][0])break;
 m_Combo.AddString(CFG.string[f]);
 }
m_Combo.SetEditSel(0,-1);
}

void CCalcWnd::SaveCombo(void)
{
char txt[101];
m_Combo.GetWindowText(txt,100);
if(!txt[0])return;
for(int f=0;f<20;f++)
 {
 if(!CFG.string[f][0])
  {
  f++;
  break;
  }
 if(stricmp(txt,CFG.string[f])==0)break;
 }

for(;f>0;f--)
 {
 if(f<20)memcpy(CFG.string[f],CFG.string[f-1],sizeof(CFG.string[0]));
 }

memcpy(CFG.string[0],txt,sizeof(CFG.string[0]));
}

void CCalcWnd::OnCalculate() 
{
if(CFG.form<0||CFG.form>3)return;
SaveCombo();
CString s;
m_pRezButton[CFG.form]->GetWindowText(s);
m_Combo.SetWindowText(s);
SaveCombo();
FillCombo();	
}

void CCalcWnd::OnClassicButton(UINT nID)
{
int n=nID-ID_CLASSIC_FIRST;

CString s1(CalcClassicButtons[n].addtobegin);
CString s2;
m_Combo.GetWindowText(s2);
m_Combo.SetWindowText(s1=(s1+s2+CalcClassicButtons[n].addtoend));
Calculate(s1);
m_Combo.SetFocus();

}


void CCalcWnd::OnCalc(UINT nID)
{
int n=nID-ID_CALC_DEC;
if(n<0||n>3)return;
SaveCombo();
CString s;
m_pRezButton[n]->GetWindowText(s);
m_Combo.SetWindowText(s);
SaveCombo();
FillCombo();	
}

void CCalcWnd::OnAppAbout() 
{

CXLayoutDialog d(&m_Font,m_iButtonWid,this);

d.DoModal();	
/*
CFrameWnd *fw=new CFrameWnd();

fw->Create(NULL,"Frame",WS_OVERLAPPEDWINDOW|WS_VISIBLE,CFrameWnd::rectDefault,this);

CXLayoutWnd *lw=new CXLayoutWnd();


CRect cr;
fw->GetClientRect(&cr);
lw->Create(_T("STATIC"),"LayoutInside",WS_CHILD|WS_VISIBLE,cr,fw,1234);
*/
m_Combo.SetFocus();
}
