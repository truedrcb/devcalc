// XDC.h: interface for the CXDC class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_XDC_H__0ECF0478_1960_4605_A7D9_05C1A468934F__INCLUDED_)
#define AFX_XDC_H__0ECF0478_1960_4605_A7D9_05C1A468934F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CXDC : public CDC  
{
CDC *paintDC;
CBitmap memBitmap,*omemBitmap;
CRect clientRect;
 
public:
CXDC();
virtual ~CXDC();

CXDC(CWnd *paintClient,CDC *paintDC);
void show(void);

};

#endif // !defined(AFX_XDC_H__0ECF0478_1960_4605_A7D9_05C1A468934F__INCLUDED_)
