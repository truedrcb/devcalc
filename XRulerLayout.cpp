// XRulerLayout.cpp: implementation of the CXRulerLayout class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DevCalc.h"
#include "XRulerLayout.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CXRulerLayout::CXRulerLayout()
{
first=0;
}

CXRulerLayout::~CXRulerLayout()
{

}

void CXRulerLayout::Create(UINT xnum,UINT ynum,const RECT rect)
{
horizontal.Create(xnum);
vertical.Create(ynum);
CXRulerLayout::rect=rect;
horizontal.SetWidth(CXRulerLayout::rect.Width());
vertical.SetWidth(CXRulerLayout::rect.Height());
}


void CXRulerLayout::Add(UINT x,UINT y,UINT w,UINT h,HWND window,UINT addh)
{
CXRulerLayout::SItem *item=new CXRulerLayout::SItem;
item->x=x;
item->y=y;
item->w=w;
item->h=h;
item->visible=TRUE;
item->addh=addh;
item->window=window;
item->next=first;
first=item;
}


void CXRulerLayout::Move(UINT x,UINT y,UINT w,UINT h,HWND window,UINT addh)
{
CXRulerLayout::SItem *item=first;
while(item)
 {
 if(item->window==window)break;
 item=item->next;
 }
if(!item)return;
item->x=x;
item->y=y;
item->w=w;
item->h=h;
item->addh=addh;
}

void CXRulerLayout::Move(UINT x,UINT y,UINT w,UINT h,HWND window)
{
CXRulerLayout::SItem *item=first;
while(item)
 {
 if(item->window==window)break;
 item=item->next;
 }
if(!item)return;
item->x=x;
item->y=y;
item->w=w;
item->h=h;
}

void CXRulerLayout::ReplaceItems(BOOL bRepaint)
{
CXRulerLayout::SItem *item;

for(item=first;item;item=item->next)
 {
 CPoint point
  (
  rect.left+horizontal.GetCellX(item->x),
  rect.top+vertical.GetCellX(item->y)
  );
 UINT w=horizontal.GetCellW(item->x,item->w);
 UINT h=vertical.GetCellW(item->y,item->h);
 if(w==0||h==0)
  {
  if(item->visible)
   {
   item->visible=FALSE;
   ShowWindow(item->window,SW_HIDE);
   }
  }
 else
  {
  CSize size
   (
   w,
   h*(1+item->addh)
   );
  CRect itemrect(point,size);
  MoveWindow(item->window,
   itemrect.left,itemrect.top,
   itemrect.Width(),itemrect.Height(),bRepaint);
  if(!item->visible)
   {
   item->visible=TRUE;
   ShowWindow(item->window,SW_SHOWNORMAL);
   }
  }
 }
}

void CXRulerLayout::Resize(const RECT rect)
{
CXRulerLayout::rect=rect;
horizontal.SetWidth(CXRulerLayout::rect.Width());
vertical.SetWidth(CXRulerLayout::rect.Height());
}
