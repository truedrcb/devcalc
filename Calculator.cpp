//#include "Calc.h"
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>

int CalculatorError=0;

long double Calc_atof(char *txt)
{
if(!*txt)return 0;
char *p=txt;
while(*p)p++;
p--;
if(*p=='h'||*p=='H')
 {
 __int64 i;
 *p=0;
 sscanf(txt,"%I64X",&i);
 return i;
 }
else
if(*txt=='$')
 {
 __int64 i;
 sscanf(txt+1,"%I64X",&i);
 return i;
 }
else
if(*txt=='0'&&(txt[1]=='x'||txt[1]=='X'))
 {
 if(!isxdigit(txt[2]))
  {
  CalculatorError=70;
  return 0;
  }
 __int64 i;
 sscanf(txt,"%I64i",&i);
 return i;
 }
else
if(*txt=='0'&&(txt[1]=='b'||txt[1]=='B'))
 {
 __int64 i=0;
 p=txt+2;
 while(*p)
  {
  i<<=1;
  if(*p!='0')i|=1;
  p++;
  }
 return i;
 }
else
if(*p=='b'||*p=='B')
 {
 __int64 i=0;
 *p=0;
 p=txt;
 while(*p)
  {
  i<<=1;
  if(*p!='0')i|=1;
  p++;
  }
 return i;
 }
else
if(strchr(txt,'.')||(strchr(txt,'e')&&tolower(txt[1])!='x'))
return atof(txt);
else
 {
 __int64 i;
 sscanf(txt,"%I64i",&i);
 return i;
 }
}

// ������� �������� � ������ �������
const char *Calc_SkipSpaces(const char *p)
{
while(*p&&(*p<=' '))p++;
return p;
}
#define SKIPSPACES {p=Calc_SkipSpaces(p);}

enum ID_types
{
T_VARIABLE,
T_BRACE_OPEN,
T_BRACE_CLOSE,
T_OPERATOR,
T_FUNCTION
};

struct Calc_IDDescStruct
{
char *name;
int type;
union
 {
 long double val;
 long double priority;
 };
void *operation;
};

#define PI 3.1415926535897932384626433832795

namespace Calc
 {
 long double Add(long double a,long double b){return a+b;}
 long double Sub(long double a,long double b){return a-b;}
 long double Mul(long double a,long double b){return a*b;}
 long double Div(long double a,long double b){return a/b;}
 long double And(long double a,long double b){return (__int64)a&(__int64)b;}
 long double Or(long double a,long double b){return (__int64)a|(__int64)b;}
 long double Xor(long double a,long double b){return (__int64)a^(__int64)b;}
 long double LShift(long double a,long double b){return (__int64)a<<(__int64)b;}
 long double RShift(long double a,long double b){return (__int64)a>>(__int64)b;}

 long double Abs(long double a){return (a<0)?(-a):(a);}
 long double Sqr(long double a){return a*a;}
 long double Not(long double a){return ~((__int64)a);}
 
 long double DegToRad(long double a){return a*PI/180.0;}
 long double RadToDeg(long double a){return a*180.0/PI;}

 long double Round(long double a)
  {
  long double f=floorl(a);
  if((a-f)>0.5)return f+1.0;
  return f;
  }
 }

struct Calc_IDDescStruct Calc_IDDesc[]=
{
{"",T_VARIABLE,0,0},
{"pi",T_VARIABLE,PI,0},
{"{",T_BRACE_OPEN,0,0},
{"[",T_BRACE_OPEN,0,0},
{"(",T_BRACE_OPEN,0,0},
{"}",T_BRACE_CLOSE,0,0},
{"]",T_BRACE_CLOSE,0,0},
{")",T_BRACE_CLOSE,0,0},
{"and",T_OPERATOR,1,Calc::And},
{"or",T_OPERATOR,1,Calc::Or},
{"xor",T_OPERATOR,1,Calc::Xor},
{"&",T_OPERATOR,1,Calc::And},
{"|",T_OPERATOR,1,Calc::Or},
{"^",T_OPERATOR,1,Calc::Xor},
{"<<",T_OPERATOR,1,Calc::LShift},
{">>",T_OPERATOR,1,Calc::RShift},
{"+",T_OPERATOR,2,Calc::Add},
{"-",T_OPERATOR,2,Calc::Sub},
{"*",T_OPERATOR,3,Calc::Mul},
{"/",T_OPERATOR,3,Calc::Div},
{"%",T_OPERATOR,3,fmodl},
{"mod",T_OPERATOR,3,fmodl},
{"pow",T_OPERATOR,4,powl},
{"**",T_OPERATOR,4,powl},
{"not",T_FUNCTION,0,Calc::Not},
{"~",T_FUNCTION,0,Calc::Not},
{"sqrt",T_FUNCTION,0,sqrtl},
{"sqr",T_FUNCTION,0,Calc::Sqr},
{"abs",T_FUNCTION,0,Calc::Abs},
{"fabs",T_FUNCTION,0,Calc::Abs},
{"ln",T_FUNCTION,0,logl},
{"log",T_FUNCTION,0,logl},
{"log10",T_FUNCTION,0,log10l},
{"exp",T_FUNCTION,0,expl},
{"floor",T_FUNCTION,0,floorl},
{"ceil",T_FUNCTION,0,ceill},
{"round",T_FUNCTION,0,Calc::Round},
{"degtorad",T_FUNCTION,0,Calc::DegToRad},
{"radtodeg",T_FUNCTION,0,Calc::RadToDeg},
{"atan",T_FUNCTION,0,atanl},
{"acos",T_FUNCTION,0,acosl},
{"asin",T_FUNCTION,0,asinl},
{"tanh",T_FUNCTION,0,tanhl},
{"cosh",T_FUNCTION,0,coshl},
{"sinh",T_FUNCTION,0,sinhl},
{"tan",T_FUNCTION,0,tanl},
{"cos",T_FUNCTION,0,cosl},
{"sin",T_FUNCTION,0,sinl}
};

// �������� ��������� ������/�������������/����� �� ������
const char *Calc_GetID(const char *p,struct Calc_IDDescStruct **Desc)
{
SKIPSPACES
char txt[500];
if(!p)return 0;
if(!*p)return 0;
char *pp=txt;
int is_digit=0;
int is_op=0;

// �������������� (������ �����)
if(isalpha(*p)||*p=='_')
for(;;)
 {
 *pp=*p;
 p++;
 pp++;
 if(!(isalpha(*p)||*p=='_'||isdigit(*p)))break;
 }
else
// ����� (����� ���������)
if(isdigit(*p)||*p=='$'||*p=='.')
 {
 is_digit=1;
 int signOK=0; // ���� ��� ���
 int pointOK=0; // ����� ��� ����
 int eOK=0; // E ��� ����
 int xOK=0; // X ��� ����
 for(;;)
  {
  *pp=*p;
  p++;
  pp++;
  if(*p<' ')break;
  else
  if(*p=='.')
   {
   if(pointOK||eOK||signOK)break;
   else pointOK=1;
   }
  else
  if((!xOK)&&(*p=='e'||*p=='E'))
   {
   if(!eOK)eOK=1;
   else break;
   }
  else
  if(*p=='+'||*p=='-')
   {
   if(eOK&&!signOK)signOK=1;
   else break;
   }
  else
  if(*p=='x'||*p=='X')
   {
   if(eOK)break;
   if(!xOK)xOK=1;
   else break;
   }
  else
  if(*p=='h'||*p=='H')
   {
   if(xOK)break;
   *pp=*p;
   p++;
   pp++;
   break;
   }
  else
  if(!isxdigit(*p))break;
  }
 }
else
// ������ �������� ����� ������
if(!strchr("[{()}]",*p))
for(;;)
 {
 is_op=1;
 *pp=*p;
 p++;
 pp++;
 if(*p<' '||!strchr("+-*/<>=?:|!^&%~",*p))break;
 }
else
// ������
 {
 *pp=*p;
 p++;
 pp++;
 }

*pp=0;
if(is_digit)
 {
 Calc_IDDesc[0].val=Calc_atof(txt);
 *Desc=Calc_IDDesc;
 return p;
 }
*Desc=0;
for(int f=1;f<(sizeof(Calc_IDDesc)/sizeof(Calc_IDDesc[0]));f++)
if(stricmp(txt,Calc_IDDesc[f].name)==0)
 {
 *Desc=Calc_IDDesc+f;
 break;
 }
// ���� �� ������� ����� �������� � ������
// ������������ ��� ������� �������
if(!*Desc)
if(is_op)
 {
 int len;
 // ���� ����� �������������� ������ �������
 while((len=strlen(txt))>1)
  {
  // ������� ��������� ������
  txt[len-1]=0;
  p-=1; 
  // �������� ����� ������������� � ������
  for(int f=1;f<(sizeof(Calc_IDDesc)/sizeof(Calc_IDDesc[0]));f++)
  if(stricmp(txt,Calc_IDDesc[f].name)==0)
   {
   *Desc=Calc_IDDesc+f;
   break;
   }
  // ���� ����� ��������
  if(*Desc)break;
  }
 }
return p;
}

// ��������� �������� ��������� �� �������� ��������� �� ������������ �����������
const char *Calc_Exp(const char *p,int priority,long double *d,int *end,int *next_priority);

// ��������� �������� ����� ����������
const char *Calc_Var(const char *p,long double *d)
{
struct Calc_IDDescStruct *Desc;
int sub=0;
p=Calc_GetID(p,&Desc);
if(!p||!Desc)
 {
 CalculatorError=20;
 return 0;
 }
if(Desc->type==T_OPERATOR)
 {
 if(Desc->operation!=Calc::Sub&&Desc->operation!=Calc::Add)
  {
  CalculatorError=30;
  return 0;
  }
 else
 if(Desc->operation==Calc::Sub)sub=1;
 p=Calc_GetID(p,&Desc);
 if(!p||!Desc)
  {
  CalculatorError=80;
  return 0;
  }
 }
if(Desc->type==T_VARIABLE)
 {
 *d=Desc->val;
 if(sub)*d=-*d;
 return p;
 }
if(Desc->type==T_FUNCTION)
 {
 p=Calc_Var(p,d);
 if(!CalculatorError)*d=((long double (*)(long double))Desc->operation)(*d);
 if(sub)*d=-*d;
 return p;
 }
if(Desc->type==T_BRACE_OPEN)
 {
 int end=0;
 p=Calc_Exp(p,0,d,&end,0);
 if(sub)*d=-*d;
 return p;
 }

CalculatorError=40;
return 0;
}

// ��������� �������� ��������� �� �������� ��������� �� ������������ �����������
const char *Calc_Exp(const char *p,int priority,long double *d,int *end,int *next_priority)
{
struct Calc_IDDescStruct *Desc;
long double a=0;
p=Calc_Var(p,d);
if(!p)return 0;

for(;;)
 {
 const char *pp=p;
 p=Calc_GetID(p,&Desc);
 if(!p)return 0;
 if(!Desc)
  {
  CalculatorError=10;
  return 0;
  }
 if(Desc->type==T_BRACE_CLOSE)
  {
  if(end)*end=1;
  return p;
  }
 if(Desc->type==T_OPERATOR)
  {
  if(Desc->priority>priority)
   {
   int next_prior;
   p=Calc_Exp(p,Desc->priority,&a,end,&next_prior);
   if(!CalculatorError)*d=
    ((long double (*)(long double,long double))Desc->operation)(*d,a);
   else return 0;
   if(!p||*end)return p;
   if(next_prior<=priority)return p;
   }
  else 
   {
   if(next_priority)*next_priority=Desc->priority;
   return pp;
   }
  }
 else
  {
  CalculatorError=50;
  return 0;
  }
 }
CalculatorError=60;
return 0;
}

long double Calculator(const char *Expression)
{
if(!Expression)return 0;
CalculatorError=0;
long double d=0;
int end=0;
Calc_Exp(Expression,0,&d,&end,0);
if(CalculatorError)return 0;
return d;
}


int LDouble2String(long double src,char *dest,int maxsize,int base)
{
maxsize-=1;
*dest=0;
int sign;
if(base<2||base>16)base=10;
int prec=24;
const char digits[16]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
if(src<0)
 {
 sign=1;
 src=-src;
 }
else sign=0;
long double hi=floorl(src);
long double lo=src-hi;

for(;;)
 {
 if(maxsize<=0)break;
 if(prec<=0)break;
 int dig=fmodl(hi,base);
 hi/=(long double)base;
 maxsize--;
 prec--;
 char *p=dest;
 for(;*p;p++);
 for(;p>=dest;p--)p[1]=p[0];
 *dest=digits[dig];
 if(hi<1)break;
 }

if(maxsize>0)
 {
 strcat(dest,".");
 maxsize--;
 }

char *p=dest;
for(;*p;p++);

for(;;)
 {
 if(maxsize<=0)break;
 if(prec<=0)break;
 lo*=(long double)base;
 long double dig=floorl(lo);
 lo-=dig;
 maxsize--;
 prec--;
 *p=digits[(int)dig];
 p++;
 *p=0;
 }

for(;;)
 {
 p--;
 if(p<=dest)break;
 if(*p=='0')
  {
  *p=0;
  }
 else
 if(*p=='.')
  {
  *p=0;
  break;
  }
 else break;
 }

return sign;
}
