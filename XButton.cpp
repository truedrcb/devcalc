// XButton.cpp : implementation file
//

#include "stdafx.h"
#include "DevCalc.h"
#include "XButton.h"
#include "XDC.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


DWORD MixColors(DWORD color1,DWORD color2,BYTE alpha)
{
int r1=GetRValue(color1);
int g1=GetGValue(color1);
int b1=GetBValue(color1);
int r2=GetRValue(color2);
int g2=GetGValue(color2);
int b2=GetBValue(color2);

return RGB(r1+((r2-r1)*alpha/255),g1+((g2-g1)*alpha/255),b1+((b2-b1)*alpha/255));
}

/////////////////////////////////////////////////////////////////////////////
// CXButton

CXButton::CXButton()
{
mx=0;
my=0;
mi=FALSE;
mb=FALSE;
state=FALSE;
m_hFont=NULL;
}

BOOL CXButton::Create(
 LPCTSTR lpszWindowName,
 DWORD dwStyle,
 const RECT& rect,
 CWnd* pParentWnd,
 UINT nID)
{
return CWnd::Create(NULL,lpszWindowName,dwStyle,rect,pParentWnd,nID);
}

BOOL CXButton::Create(
 UINT rIDMetafile,
 LPCTSTR lpszWindowName,
 DWORD dwStyle,
 const RECT& rect,
 CWnd* pParentWnd,
 UINT nID)
{
if(rIDMetafile)m_MetaFile.Load(rIDMetafile);
return CWnd::Create(NULL,lpszWindowName,dwStyle,rect,pParentWnd,nID);
}

BOOL CXButton::Create(
 UINT rIDMetafile,
 DWORD dwStyle,
 const RECT& rect,
 CWnd* pParentWnd,
 UINT nID)
{
m_MetaFile.Load(rIDMetafile);
return CWnd::Create(NULL,"",dwStyle,rect,pParentWnd,nID);
}

CXButton::~CXButton()
{
}


BEGIN_MESSAGE_MAP(CXButton, CWnd)
	//{{AFX_MSG_MAP(CXButton)
	ON_WM_PAINT()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_CAPTURECHANGED()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CXButton message handlers

void CXButton::OnPaint() 
{
CPaintDC paintDC(this); // device context for painting

// Do not call CWnd::OnPaint() for painting messages
CRect r,clir;
GetClientRect(&r);

DWORD style=GetStyle();
DWORD align=style&XB_ALIGN;

CXDC memDC(this,&paintDC);

clir=r;
r.OffsetRect(-r.left,-r.top);

DWORD colorShadow=MixColors(GetSysColor(COLOR_3DSHADOW),
 				       GetSysColor(COLOR_3DFACE),70);
if(mb)
 memDC.FillSolidRect(&r,colorShadow);
else
 {
 if(state)
  memDC.FillSolidRect(&r,MixColors(GetSysColor(COLOR_3DHILIGHT),
 				       GetSysColor(COLOR_3DFACE),160));
 else
  memDC.FillSolidRect(&r,GetSysColor(COLOR_3DFACE));
 }

CPen penBlue(PS_NULL, 5, RGB(0, 0, 255));
CPen* pOldPen = memDC.SelectObject(&penBlue);

CBrush brushRed(GetSysColor(COLOR_BTNTEXT));
CBrush* pOldBrush = memDC.SelectObject(&brushRed);

if(mb)
 {
 CRect rr;
 rr=r;
 CBrush brushShadow1(MixColors(colorShadow,
  				       GetSysColor(COLOR_3DFACE),120));
 memDC.SelectObject(&brushShadow1);

 CPoint point(3,3);
 rr.DeflateRect(3,3,0,0);
 memDC.RoundRect(rr,point); 

 CBrush brushShadow2(MixColors(colorShadow,
  				       GetSysColor(COLOR_3DFACE),150));
 memDC.SelectObject(&brushShadow2);

 rr.DeflateRect(1,1,0,0);
 memDC.RoundRect(rr,point); 

 memDC.SelectObject(&brushRed);
 }


 {
 CRect rr;
 rr=r;
 CFont *of=NULL;
 if(m_hFont)
  {
  of=memDC.SelectObject(CFont::FromHandle(m_hFont));
  }
 CString str;
 GetWindowText(str);
 memDC.SetBkMode(TRANSPARENT);
 memDC.SetTextColor(GetSysColor(COLOR_BTNTEXT));
 //memDC.DrawText(str,&rr,DT_VCENTER|DT_CENTER|DT_SINGLELINE);
 CRect tr=rr;
 int th=memDC.DrawText(str,&tr,DT_CALCRECT|DT_WORDBREAK);
 CPoint point(rr.left,rr.Height()/2-th/2);
 if(align==XB_RIGHT)
  {
  point.Offset(rr.Width()-tr.Width(),0);
  }
 else
 if(align!=XB_LEFT)
  {
  point.Offset(rr.Width()/2-tr.Width()/2,0);
  }
 CRect or(point,tr.Size());
 if(mb)or.OffsetRect(0,1);
 memDC.DrawText(str,&or,DT_WORDBREAK);

 if(of)memDC.SelectObject(of);
 }

memDC.SelectObject(pOldBrush);
memDC.SelectObject(pOldPen);
/*
if(mi||mb)
 {
 DWORD col_hi=MixColors(GetSysColor(COLOR_3DHILIGHT),
 					    GetSysColor(COLOR_3DLIGHT),100);
 DWORD col_lo=MixColors(GetSysColor(COLOR_3DDKSHADOW),
					    GetSysColor(COLOR_3DSHADOW),210);
 memDC.Draw3dRect(&r,col_lo,col_hi);
 }
*/
if(!(style&WS_DISABLED))
 {

 DWORD col_hi=MixColors(GetSysColor(COLOR_3DHILIGHT),
 					    GetSysColor(COLOR_3DLIGHT),100);
 DWORD col_lo=MixColors(GetSysColor(COLOR_3DDKSHADOW),
					    GetSysColor(COLOR_3DSHADOW),210);
 if(!(mi||mb))
  {
  col_hi=MixColors(col_hi,GetSysColor(COLOR_3DFACE),180);
  col_lo=MixColors(col_lo,GetSysColor(COLOR_3DFACE),180);
  }
 memDC.Draw3dRect(&r,col_lo,col_hi);
 }

if(!m_MetaFile.IsEmpty())
 {
 clir.DeflateRect(4,4,5,5);
 if(mb)clir.OffsetRect(0,1);
 METAALIGNMENT al=AlignFit;
 if(align==XB_LEFT)
  al=AlignFitLeft;
 else
 if(align==XB_RIGHT)
  al=AlignFitRight;
 else
 if(align==XB_STRETCH)
  al=AlignStretch;

 m_MetaFile.Display(&memDC,clir,al);
 }

memDC.show();
}

void CXButton::OnMouseMove(UINT nFlags, CPoint point) 
{
mx=point.x;
my=point.y;
CRect r;
GetClientRect(&r);
if(mi)
 {
 if(!r.PtInRect(point))
  {
  mi=FALSE;
  mb=FALSE;
  ReleaseCapture();
  Invalidate();
  }
 }
else
 {
 mi=TRUE;
 SetCapture();
 Invalidate();
 }

CWnd::OnMouseMove(nFlags, point);
}

void CXButton::OnLButtonDown(UINT nFlags, CPoint point) 
{
if(!mb)
 {
 mb=TRUE;	
 Invalidate();
 }
CWnd::OnLButtonDown(nFlags, point);
}

void CXButton::OnLButtonUp(UINT nFlags, CPoint point) 
{
if(mb)
 {
 GetParent()->PostMessage(WM_COMMAND,GetDlgCtrlID());
 mb=FALSE;
 ReleaseCapture();
 mi=FALSE;
 Invalidate();
 }
CWnd::OnLButtonUp(nFlags, point);
}

LRESULT CXButton::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
if(message==WM_SETFONT)
 {
 m_hFont=(HFONT)wParam;
 }
else
if(message==WM_SETTEXT)
 {
 Invalidate();
 }
return CWnd::DefWindowProc(message, wParam, lParam);
}

void CXButton::SetState( BOOL bHighlight)
{
if(bHighlight!=state)
 {
 state=bHighlight;
 Invalidate();
 }
}

void CXButton::OnCaptureChanged(CWnd *pWnd) 
{
if(mi&&pWnd!=this)
 {
 mi=FALSE;
 mb=FALSE;
 Invalidate();
 }
CWnd::OnCaptureChanged(pWnd);
}
