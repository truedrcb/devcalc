# DevCalc - Calculator for Developers
ver. 2.021

![Screenshot](https://bitbucket.org/repo/8M855j/images/463433004-DevCalc202.gif)

Designed for Windows95/98.

Calculates value of math expression entered in form of string
(like in high level language program).

## Installation
Just copy DevCalc.exe to any directory. DEVCALC.cfg will be automatically made in this directory.
To uninstall - just remove files DevCalc.exe and DEVCALC.cfg.

## Troubles
If there is any problem with DevCalc appearance (wrong fonts and so on) - just remove DEVCALC.cfg file.
All options will be set default.


## Now supported:

+ operation hierarchy ("multiply" vs "add" and etc.). Examples:
	* `2+2*3` = `2+(2*3)` = `8`
	* `(2+2)*3**2` = `(2+2)*(3**2)` = `36`
- any form of braces 
	* `{`, `[`, `(`, `}`, `]`, `)`

- constants:
	* `pi`

- numbers in C, ASM and Pascal formats:
	* `[-|+]12345[.12345][e[-|+]12345]` - decimal
	* `0101101b|0b0101101` - binary
	* `0x12345|12345h|12345$` - hexadecimal
	* `01234|1234o` - octal
	* __All numbers MUST begin with digit!__ Examples:
		* `12.3`
		* `0x12`
		* `0feh` - correct
		* `feh` - wrong

- functions - `[function][value]`:
	* examples:
		* `sin(5)`
		* `~2`
	* list:
		* `~` or `not` - bitwise NOT
		* `degtorad` - convert degrees to radians
		* `radtodeg` - convert radians to degrees
		* `sin`, `cos`, `tan`, `asin`, `acos`, `atan` - trigonometric functions
		* `sinh`, `cosh`, `tanh` - hyperbolic functions same as in C/C++
		* `log`, `log10`, `exp`, `sqrt`, `fabs` - same as in C/C++
		* `ln`, `sqr`, `abs` - same as in Pascal

- operations - `[value1][operation][value2]`:
	* examples:
		* `2+pi`
		* `0x200<<3`
		* `0ff$ xor 0c5$`
	* list:
		* `+`, `-`, `*`, `/`, `%`, `^`, `|`, `&`, `>>`, `<<` - like in C/C++
		* `and`, `or`, `xor`, `mod` - like in Pascal
		* `pow`, `**` - raising in power

Some operations deal with double precision and some with long double.

Before and after operation all values are converted to long double.
This means that length of all numbers limited by double or (in some cases) long double precision.

* mailto:  artem@gratchev.net
* http://gratchev.net

