// DevCalcDlg.h : header file
//

#if !defined(AFX_DEVCALCDLG_H__E33D1187_EB69_11D3_B481_0000E8550785__INCLUDED_)
#define AFX_DEVCALCDLG_H__E33D1187_EB69_11D3_B481_0000E8550785__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CDevCalcDlg dialog

class CDevCalcDlg : public CDialog
{
// Construction
public:
	CDevCalcDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CDevCalcDlg)
	enum { IDD = IDD_DEVCALC_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDevCalcDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CDevCalcDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEVCALCDLG_H__E33D1187_EB69_11D3_B481_0000E8550785__INCLUDED_)
