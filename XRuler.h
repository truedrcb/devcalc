// XRuler.h: interface for the CXRuler class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_XRULER_H__6197F3FF_5243_49AA_ADD8_9A2CE6E1D6C7__INCLUDED_)
#define AFX_XRULER_H__6197F3FF_5243_49AA_ADD8_9A2CE6E1D6C7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CXRuler  
{
struct SCell
 {
 UINT width;
 UINT weight;
 BOOLEAN visible;
 UINT w;
 UINT minwidth;
 };

struct SCell *cells;
UINT size;

UINT width;

void CalcW();

public:
CXRuler();
virtual ~CXRuler();

void Close();
void Create(UINT size);

void SetCellWidth(UINT index,UINT width);
void SetCellMinWidth(UINT index,UINT width);
void SetCellWeight(UINT index,UINT weight);
void SetCellVisible(UINT index,BOOLEAN visible=TRUE);
UINT GetMinWidth();
UINT GetCellWeight(UINT index);
UINT GetCellWidth(UINT index);
UINT GetCellW(UINT index);
UINT GetCellX(UINT index);
UINT GetCellW(UINT index,UINT num);
UINT GetWidth();
void SetWidth(UINT width);

};

#endif // !defined(AFX_XRULER_H__6197F3FF_5243_49AA_ADD8_9A2CE6E1D6C7__INCLUDED_)
