#if !defined(AFX_CALCWND_H__10F6A7A1_EF87_11D3_B481_0000E8550785__INCLUDED_)
#define AFX_CALCWND_H__10F6A7A1_EF87_11D3_B481_0000E8550785__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CalcWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCalcWnd window

#include "XButton.h"
#include "XRulerLayout.h"
#include "XLayoutWnd.h"
#include "MetaFile.h"

class CCalcWnd : public CWnd
{
struct SClassicButtonFunction
 {
 LPCTSTR name;
 UINT rID;
 LPCTSTR addtobegin;
 LPCTSTR addtoend;
 };

CXRulerLayout layout;

//CMetaFile m_TitleWMF;

HACCEL m_hAccel;
CFont m_Font;
CXButton m_ExitButton;
CXButton m_MenuButton;
CComboBox m_Combo;
CXButton *m_pRezButton[4];
CXButton *m_pSelectButton[4];
CMenu m_Menu;
CXLayoutWnd m_ClassicWnd;

CToolTipCtrl m_ToolTip;
//CStatic m_TitleStatic;
CXButton m_TitleStatic;

BOOLEAN m_bInitialized;

int m_iBordWid;
int m_iButtonWid;

// Construction
public:
	CCalcWnd();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCalcWnd)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
public:
virtual ~CCalcWnd();
void RecalcLayout(void);
void CreateFont(void);
void CreateShow(void);
void Calculate(LPCTSTR txt);
void FillCombo(void);
void SaveCombo(void);

	// Generated message map functions
protected:
	//{{AFX_MSG(CCalcWnd)
	afx_msg void OnPaint();
	afx_msg UINT OnNcHitTest(CPoint point);
	afx_msg void OnDestroy();
	afx_msg void OnAppExit();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMenu();
	afx_msg void OnCalculate();
	afx_msg void OnAppAbout();
	//}}AFX_MSG

afx_msg void OnChangeFont();
afx_msg void OnShow(UINT nID);
afx_msg void OnComboChange();
afx_msg void OnComboSelChange();
afx_msg void OnSelect(UINT nID);
afx_msg BOOL OnToolTipNotify(UINT id, NMHDR *pNMHDR,LRESULT *pResult);
afx_msg void OnCalc(UINT nID);
afx_msg void OnClassicButton(UINT nID);

DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CALCWND_H__10F6A7A1_EF87_11D3_B481_0000E8550785__INCLUDED_)
