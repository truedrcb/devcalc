#if !defined(AFX_XLAYOUTDIALOG_H__14F2FAC4_5150_4968_92E0_A1E2CD8297B2__INCLUDED_)
#define AFX_XLAYOUTDIALOG_H__14F2FAC4_5150_4968_92E0_A1E2CD8297B2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// XLayoutDialog.h : header file
//

#include "XLayoutWnd.h"
#include "XButton.h"

/////////////////////////////////////////////////////////////////////////////
// CXLayoutDialog dialog

class CXLayoutDialog : public CDialog
{
//CStatic s1;

CXButton m_ExitButton;
CXButton m_TitleStatic;
CXButton m_ContentStatic;

CXLayoutWnd layout;

CFont *m_Font;
int m_iButtonWid;

// Construction
public:
	CXLayoutDialog(CFont *Font, int iButtonWid=20, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CXLayoutDialog)
	enum { IDD = IDD_ABOUTDIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXLayoutDialog)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:

 void CenterWindow();	

protected:

	// Generated message map functions
	//{{AFX_MSG(CXLayoutDialog)
	afx_msg UINT OnNcHitTest(CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XLAYOUTDIALOG_H__14F2FAC4_5150_4968_92E0_A1E2CD8297B2__INCLUDED_)
